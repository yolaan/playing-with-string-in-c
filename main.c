#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#define SIZE 10
char caractere();
char constCaractere();
char * arrayOfChar(int size);
char * string(int size);
char * createDynamicString(size_t size);
char * changeDynamicString(size_t size, char *str);
void printstr(char * str);
char * randConstStr(int size);
char * car20length();
char * tab10str(char ** arr);
char * constStr255(char ** arr);
char ** createDynamicArrayString(int nb);
char ** reallocateArrayString(char ** arrayOfStr,int sizeOfStr,int newSizeOfStr);
char * dynStrEssai();
char * reallocDynStr(char * dynstr);
char * strEssai();
char ** arrayOfEmptyStr(int nbOfStr);


char caractere()
{
    char caractere = rand() % (122 + 1 - 97) + 97;
    return caractere;
}
char constCaractere()
{
    const char caractere = rand() % (122 + 1 - 97) + 97;
    return caractere;
}
char * arrayOfChar(int size)
{
    size++;
    char * ptr;
    char array[size];
    int i = 0;
    for (; i < size; i++)
    {
        array[i] = caractere();
    }
    ptr = array;

    return ptr;


}
char * string(int size)
{
    char * str;
    size++; // add for '\0'
    str = (char *) malloc(sizeof(char) * size + 1);

    if(size == 1)
    {
        str[0] = caractere();
        str[1] = '\0';
        return str;
    }

    int i = 0;
    for(;i<size-1;i++)str[i] = caractere();

    str[size - 1] = '\0';
    return str;
}
char * createDynamicString(size_t size)
{

    char * ReturnPtr = NULL;
    printf("\n\tEnter a string of %zu length: \n",size);
    size++; // '/0'

    ReturnPtr = (char *) malloc(sizeof(char)*size);

    ReturnPtr = fgets(ReturnPtr,(int)size,stdin);

    ReturnPtr[size] = '\0';

    return ReturnPtr;
}
char * changeDynamicString(size_t size, char *str)
{
    if (str == NULL)
    {
        printf("String is empty, enter something in it:");
        str = createDynamicString(size);
        return str;

    }
    else
    {
        str = realloc(str,size*sizeof(char));
        printf("new size of string is: %d\n",(int)size);
        return str;
    }
}
void printstr(char * str)
{
    int c = 0;
    do
    {
        printf("---------------\n|");
        printf("%p\t| --> \t", (void*)&str[c]);
        printf(" %c |\n",str[c]);
        printf("--------------");
        ++c;
    }while(str[c] != '\0');
}
char * randConstStr(int size)
{
    char * ptr = string(size);
    return ptr;
}
char * car20length()
{
    char * str = malloc(sizeof(char)*21);
    str = string(20);
    str[strlen(str)] = '\0';
    return str;

}
char * tab10str(char ** arr)
{
    int i = 0;
    for(;i<10;i++)
    {
        int a = rand()%30;
        arr[i] = malloc(a * sizeof(char));
        arr[i] = string(a);
    }
    return * arr;
}
char * constStr255(char ** arr)
{
    int i = 0, j = 0;
    for(;i<255;i++)
    {
        arr[i] = malloc(sizeof(char)*100);
        if(arr[i] == NULL) realloc(arr[i],sizeof(char)*100);
        arr[i] = string(98);
    }
    return * arr;
}
char ** createDynamicArrayString(int nb)
{
    char ** arrayOfStr;
    arrayOfStr = (char**) malloc(sizeof(char*) * nb);
    int i = 0;
    for(;i<nb;i++)
    {
        arrayOfStr[i] = string(rand()%10+1);
    }
    return arrayOfStr;
}
char ** reallocateArrayString(char ** arrayOfStr,int sizeOfStr,int newSizeOfStr)
{
    if(arrayOfStr == NULL)
    {
        printf("String NULL - Creating string of %d size\n",newSizeOfStr);
        char ** ptr = createDynamicArrayString(newSizeOfStr);
        return ptr;
    }
    else
    {
        arrayOfStr = realloc(*arrayOfStr,newSizeOfStr);
        int i = sizeOfStr;
        for(;i<newSizeOfStr;i++) arrayOfStr[i] = string(rand()%10+1);
        return arrayOfStr;

    }
}
char * dynStrEssai()
{
    char * dynstr = (char *)malloc(sizeof(char)*strlen("ESSAI"));
    strcpy(dynstr,"ESSAI");
    return dynstr;
}
char * reallocDynStr(char * dynstr)
{
    dynstr = (char *)realloc(dynstr, sizeof(char)*strlen("ESSAI DE STRING DYNAMIQUE"));
    strcat(dynstr," DE STRING DYNAMIQUE");
    return dynstr;
}
char * strEssai()
{
    char essaie[20] = "ESSAI";
    char * ptr = essaie;
    return ptr;
}
char ** arrayOfEmptyStr(int nbOfStr)
{
    char ** arrOfEmptyStr;
    arrOfEmptyStr = (char **)malloc(sizeof(char*)*nbOfStr);

    int i = 0;
    for(;i<nbOfStr;i++)
    {
        arrOfEmptyStr[i] = malloc(sizeof(char)*100); // init à 100 pour un string (reallouable plus tard)
        arrOfEmptyStr[i] = '\0';
    }
    return arrOfEmptyStr;
}




int main()
{
    srand(time(NULL));
    int i = 0;



    printf("Si vous avez des doutes sur les constructions de chaînes et de caractères, essayez de construire les déclarations\n"
           "suivantes :\n\n");


    //Caractère

    printf("Caractère ---> \t %c", caractere());



    // Tableau de N Caractères



    printf("\n\n tableau de caractère de taille %d \n\n ",SIZE);
    char * arr = arrayOfChar(SIZE);
    i = 0;
    for(;i<10;i++)printf("array[%d] --> %c \n",i,arr[i]);
    printf("\n\n");


    // Chaine de caractère dynamique


    printf("Chaine de caractère dynamique de taille %d ----> \t %s",SIZE,createDynamicString(SIZE));

    // Constante chaine de caractère


     printf("\n\n constante chaine de caractère: %s",randConstStr(SIZE));


     //chaine de caractère de longueur 20


    printf("\n\nchaine de caractère de longueur 20 ---> \t %s",car20length());

    // tableau de 10 chaînes de caractères


    char * ptr[10];
    *ptr = tab10str(ptr);
    i = 0;
    //affichage
    printf("10 string dans un tableau : \n");
    for(;i<10;i++)printf("arr[%d] : \t %s\n",i,ptr[i]);
    free(*ptr);

    // 255 constante chaine de caractère

    char * arrStr[255];
    * arrStr = constStr255(arrStr);
    i = 0;
    for(;i<255;i++)
    {
        printf("\n%s",arrStr[i]);
        free(arrStr[i]);
    }
    free(*arrStr);



    // tableau dynamique de chaine de caractère
    printf("\n\n tableau dynamique de 5 str : \n");
    int nbOfString= 2;

    char ** arrayOfStr = createDynamicArrayString(nbOfString);
    for(;i<nbOfString;i++) printf("%s\n",arrayOfStr[i]);

    int start = nbOfString;
    int newNbOfStr = 5;
    printf("\nReallocation du tab de String pour faire un tab dynamique de 5 str ");
    arrayOfStr = reallocateArrayString(arrayOfStr,start,newNbOfStr);


    for(;i<newNbOfStr;i++)
    {
        printf("\n\t\t%s",arrayOfStr[i]);
        free(arrayOfStr[i]);
    }

    printf("\n\n");
    free(arrayOfStr);

    // test if ptr == NULL
    nbOfString= 5;

    char ** ptr2 = NULL;
    ptr2 = reallocateArrayString(ptr2,0,nbOfString);
    i = 0;
    for(;i<nbOfString;i++)
    {
        printf("%s\n",ptr2[i]);
        free(ptr2[i]);
    }
    free(ptr2);

    // dynamic string init to "ESSAI"


    printf("1. %s \n",dynStrEssai());

    printf("2. %s",reallocDynStr(dynStrEssai()));
    free(reallocDynStr(dynStrEssai()));

    // chaine de caractère de long 20 initialisé à essai
    printf("%s",strEssai());

    // tableau de chaînes de caractères, initialisées à la chaîne vide
    char ** emptyStr = arrayOfEmptyStr(150);

    // tableau statique à 2 dimensions de chaînes de caractères. SANS FONCTION.
    static char staticArrOfStr[10][20];
    i = 0;
    for(;i<10;i++)
    {
        strcpy(staticArrOfStr[i],string(20));
        printf("%s\n",staticArrOfStr[i]);

    }






    return 0;
}